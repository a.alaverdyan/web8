function open_form(){
    history.pushState(null, null, "#form");
    document.getElementById("id_form1").style.display = "block";
    document.getElementById("id_form1").style.position = "fixed"
    document.getElementById("id_form1").style.top = "0";
    document.getElementById("id_form1").style.left = "0";
    document.getElementById("id_form1").style.width = "100%";
    document.getElementById("id_form1").style.height = "100%";
    document.getElementById("id_form1").style.backgroundColor = "rgba(100, 100, 100, 0.5)";
}

function close_form(){
    document.getElementById("id_form1").style.display = "none";
}

document.addEventListener ('keydown', function(evt) {
    if(evt.keyCode == 27)
        close_form();
})

window.addEventListener("popstate", function(e) {
    if(location.hash != "#form")
        {
            document.getElementById("id_form1").style.display = "none";
        }
});